<?php

namespace Trivago\Recruiting\Service;
/**
 * The implementation is responsible for resolving the id of the city from the
 * given city name (in this simple case via an array of CityName => id). The second 
 * responsibility is to sort the returning result from the partner service in whatever
 * way. 
 * 
 * This breaks with the rule of the separation of concerns, but for this test case we want to
 * keep it simple.
 *
 * @author mmueller
 */
abstract class AbstractHotelService
{

	/**
	 * @var PartnerServiceInterface
	 */
	protected $oPartnerService;

	/**
	 * Maps from city name to the id for the partner service.
	 *  
	 * @var array
	 */
	protected $aCityToIdMapping = array(
	        "Düsseldorf" => 14575
	    );

	/**
	 * @param PartnerServiceInterface $oPartnerService
	 */
	public function __construct(PartnerServiceInterface $oPartnerService)
	{
	   $this->oPartnerService = $oPartnerService;

	}


    /**
     * @param string $sCityName Name of the city to search for.
     *
     * @return \Trivago\Recruiting\Entity\Hotel[]
     * @throws \InvalidArgumentException if city name is unknown.
     */
    abstract public function getHotelsForCity($sCityName);
}