<?php
namespace Trivago\Recruiting\Entity;

/**
 * Represents a single hotel in the result.
 *
 * @author mmueller
 */
class Hotel 
{
    /**
     * Name of the hotel.
     *
     * @var string
     */
    public $sName;

    /**
     * Street adr. of the hotel.
     * 
     * @var string
     */
    public $sAdr;

    /**
     * Unsorted list of partners with their corresponding prices.
     * 
     * @var Partner[]
     */
    public $aPartners = array();



    public function addPartners($partner){
        array_push($this->aPartners, $partner);
    }

    /**
     * @param $name $address
     */
     public function __construct($name, $address)
     {
        $this->sName = $name;
        $this->sAdr = $address;
    }



}