<?php
namespace Trivago\Recruiting\Service;


/**
 * This class implementation of an Name Ordered hotel service.
 *
 * @author pulkit gupta
 */
class NameOrderedHotelService extends AbstractHotelService
{

    /**
     * @abstract function to get sorted array of hotels[]
     */
    public function getHotelsForCity($sCityName)
    {
        if (!isset($this->aCityToIdMapping[$sCityName]))
        {
            throw new \InvalidArgumentException(sprintf('Given city name [%s] is not mapped.', $sCityName));
        }

        $iCityId = $this->aCityToIdMapping[$sCityName];
        
        //get unordered
        $aPartnerResults = $this->oPartnerService->getResultForCityId($iCityId);

        usort($aPartnerResults, function($a, $b){
            //change here to sub arrays for comparisions.
            return strnatcmp($a->sName, $b->sName);
        });

        return $aPartnerResults;

    }
}



