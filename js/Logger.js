

function CountingLogger() {
	    this.events = { };
}

(function(){


	CountingLogger.prototype.log = function(eventName) {
	    if (typeof this.events[eventName] === 'undefined') {
	        this.events[eventName] = 0;
	    }
	    this.events[eventName]++;
	};

})()
